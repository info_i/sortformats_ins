# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression.
Use the insertion algorithm"""

import sys

# Ordered list of image formats, from lower to higher insertion
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1, format2):
    # función para comparar distintos formatos con su indice

    return fordered.index(format1) < fordered.index(format2)


def find_lower_pos(formats, pivot):
    # función para encontrar la posicion más baja
    lower_pos = pivot + 1
    for i in range(pivot + 2, len(formats)):
        if lower_than(formats[i], formats[lower_pos]):
            lower_pos = i
    return lower_pos


def sort_pivot(formats, pivot):
    # función para ordenar los formatos según la lista

    while pivot > 0 and not lower_than(formats[pivot - 1], formats[pivot]):
        formats[pivot - 1], formats[pivot] = formats[pivot], formats[pivot - 1]
        pivot -= 1
    return pivot


def sort_formats(formats):
    #funcion para iterar

    for i in range(1, len(formats)):
        sort_pivot(formats, i)
    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()
